package com.example.wbb

import android.content.Context
import android.util.Log
import com.herumi.mcl.*
import java.security.MessageDigest
import java.util.*
import java.util.concurrent.TimeUnit

class CryptoCore(context: Context?) {
    protected var context: Context

    suspend fun init() {
        Mcl.SystemInit(MclConstants.BN254)

        val g1 = G1()
        g1.setStr(BN254_g1_hex, 16)
        val g2 = G2()
        g2.setStr(BN254_g2)
        val m1 = Fr("1b2aed725be94eaf356249331ed2ba2257bda9c3839dffdd97b6e6072ddef468", 16)
        val m2 = Fr("12173229811363755910129794396782386209193297576437260389920781972141044888253")
        val x0 = Fr("1a187d05038ee5507321059ed738094293a149267468cedc64cdb0fb72ca5e3b", 16)
        val x1 = Fr("12b45de28906d8ed87e757dcc612a02eec752e4ae68762a996a6a935c73bead8", 16)
        val x2 = Fr("632249191510266481480053006281881645504806761449316089045698609678595464394")

        DataStoreManager.saveValue(context, DataStoreManager.M1, m1.toString())
        DataStoreManager.saveValue(context, DataStoreManager.M2, m2.toString())
        DataStoreManager.saveValue(context, DataStoreManager.X0, x0.toString())
        DataStoreManager.saveValue(context, DataStoreManager.X1, x1.toString())
        DataStoreManager.saveValue(context, DataStoreManager.X2, x2.toString())
        DataStoreManager.saveValue(context, DataStoreManager.G1, g1.toString())
        DataStoreManager.saveValue(context, DataStoreManager.G2, g2.toString())

    }


    companion object {
        private const val BN254_g1_hex =
            "1 2523648240000001BA344D80000000086121000000000013A700000000000012 0000000000000000000000000000000000000000000000000000000000000001"

        private const val BN254_g2 =
            "1 12723517038133731887338407189719511622662176727675373276651903807414909099441 4168783608814932154536427934509895782246573715297911553964171371032945126671 13891744915211034074451795021214165905772212241412891944830863846330766296736 7937318970632701341203597196594272556916396164729705624521405069090520231616"

        private fun SHA256(input: String): String {
            return MessageDigest
                .getInstance("SHA-256")
                .digest(input.toByteArray())
                .fold("", { str, it -> str + "%02X".format(it) })
        }
    }

    init {
        this.context = context!!.applicationContext
    }

    init {
        System.loadLibrary("mcljava")
    }
}