package com.example.wbb.data

import androidx.lifecycle.LiveData

class DataRepository(private val dataDao: DataDao) {

    val readAllData: LiveData<List<Data>> = dataDao.readAllData()

    fun getValue(name: String): String {
        return dataDao.getValue(name)
    }

    suspend fun addData(data: Data){
        dataDao.addData(data)
    }
}