package com.example.wbb.data

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface DataDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addData(data: Data)

    @Query("SELECT * FROM crypto_data ORDER BY id ASC")
    fun readAllData(): LiveData<List<Data>>

    @Query("SELECT value  FROM crypto_data WHERE name LIKE :name LIMIT 1")
    fun getValue(name: String): String
}