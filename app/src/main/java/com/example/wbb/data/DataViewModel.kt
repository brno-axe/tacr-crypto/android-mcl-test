package com.example.wbb.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DataViewModel(application: Application): AndroidViewModel(application) {

    private val readAllData: LiveData<List<Data>>
    private val repository: DataRepository

    init {
        val dataDao = CryptoDatabase.getDatabase(application).dataDao()
        repository = DataRepository(dataDao)
        readAllData = repository.readAllData
    }

    fun addData(data: Data){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addData(data)
        }
    }

    fun getValue(name: String): String {
        return repository.getValue(name)
    }

}