package com.example.wbb

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.herumi.mcl.*
import java.math.BigInteger
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class CryptoTest(context: Context?) {
    protected var context: Context
    fun schnorrTest(): String {
        Mcl.SystemInit(MclConstants.BN254)
        val x = Fr("12b45de28906d8ed87e757dcc612a02eec752e4ae68762a996a6a935c73bead8", 16)
        val m = Fr("1b2aed725be94eaf356249331ed2ba2257bda9c3839dffdd97b6e6072ddef468", 16)
        val tmp = Fr()
        val g1 = G1()
        g1.setStr(BN256_g1_hex, 16)
        //m+x
        Mcl.add(tmp, m, x)
        //1/m+x
        Mcl.div(tmp, Fr(1), tmp)
        //Sigma
        val sigma = G1()
        Mcl.mul(sigma, g1, tmp)

        //generating random nonce, r, ror, rom
        val nonce = Fr()
        nonce.setByCSPRNG()
        val r = Fr()
        r.setByCSPRNG()
        val ror = Fr()
        ror.setByCSPRNG()
        val rom = Fr()
        rom.setByCSPRNG()

        //sigma_roof
        val sigma_roof = G1()
        Mcl.mul(sigma_roof, sigma, r)

        //t
        val t = G1()
        Mcl.mul(t, sigma_roof, rom)
        val temp_g = G1()
        Mcl.mul(temp_g, g1, ror)
        Mcl.add(t, t, temp_g)

        //e
        var hash =
            SHA1(mclCurvePointToDatabase(sigma_roof) + mclCurvePointToDatabase(t) + toHex(nonce.toString()))
        val e = Fr()
        e.setStr(hash, 16)

        //sr
        val sr = Fr()
        Mcl.mul(sr, e, r)
        Mcl.add(sr, ror, sr)

        //sm
        val sm = Fr()
        Mcl.mul(sm, e, m)
        Mcl.sub(sm, rom, sm)

        //t_dash
        val t_dash = G1()
        //g1^sr
        Mcl.mul(t_dash, g1, sr)
        Mcl.mul(tmp, e, x)
        Mcl.sub(tmp, sm, tmp)
        Mcl.mul(temp_g, sigma_roof, tmp)
        Mcl.add(t_dash, t_dash, temp_g)
        hash =
            SHA1(mclCurvePointToDatabase(sigma_roof) + mclCurvePointToDatabase(t_dash) + toHex(nonce.toString()))
        val e_dash = Fr(hash, 16)

        return if (e_dash.equals(e)) {
            Toast.makeText(context, "Working", Toast.LENGTH_SHORT).show()
            return "Schnorr/SUCCESS/$hash"
        } else {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
            return ""
        }
    }

    fun wbbTest(): String {
        Mcl.SystemInit(MclConstants.BN254)
        val g2 = G2()
        g2.setStr("1 12723517038133731887338407189719511622662176727675373276651903807414909099441 4168783608814932154536427934509895782246573715297911553964171371032945126671 13891744915211034074451795021214165905772212241412891944830863846330766296736 7937318970632701341203597196594272556916396164729705624521405069090520231616")
        val x = Fr("12b45de28906d8ed87e757dcc612a02eec752e4ae68762a996a6a935c73bead8", 16)
        val m = Fr("1b2aed725be94eaf356249331ed2ba2257bda9c3839dffdd97b6e6072ddef468", 16)
        //Fr x0 = new Fr("1a187d05038ee5507321059ed738094293a149267468cedc64cdb0fb72ca5e3b", 16);
        val tmp = Fr()
        val gx = G2()
        Mcl.mul(gx, g2, x)
        val g1 = G1()
        g1.setStr(BN256_g1_hex, 16)
        //m+x
        Mcl.add(tmp, m, x)
        //1/m+x
        Mcl.div(tmp, Fr(1), tmp)
        //Sigma
        val sigma = G1()
        Mcl.mul(sigma, g1, tmp)

        //generating random nonce, r, ror, rom
        val nonce = Fr()
        nonce.setByCSPRNG()
        val r = Fr()
        r.setByCSPRNG()
        val ror = Fr()
        ror.setByCSPRNG()
        val rom = Fr()
        rom.setByCSPRNG()

        //sigma_roof
        val sigma_roof = G1()
        Mcl.mul(sigma_roof, sigma, r)

        //sigma_plane
        val temp_g = G1()
        val sigma_plane = G1()
        Mcl.neg(tmp, m)
        Mcl.mul(sigma_plane, sigma_roof, tmp)
        Mcl.mul(temp_g, g1, r)
        Mcl.add(sigma_plane, sigma_plane, temp_g)

        //t
        val t = G1()
        Mcl.mul(t, sigma_roof, rom)
        Mcl.mul(temp_g, g1, ror)
        Mcl.add(t, t, temp_g)

        //e
        val hash =
            SHA1(mclCurvePointToDatabase(sigma_roof) + mclCurvePointToDatabase(t) + toHex(nonce.toString()))
        val e = Fr()
        e.setStr(hash, 16)

        //sr
        val sr = Fr()
        Mcl.mul(sr, e, r)
        Mcl.add(sr, ror, sr)

        //sm
        val sm = Fr()
        Mcl.mul(sm, e, m)
        Mcl.sub(sm, rom, sm)

        //t_dash
        val t_dash = G1()
        //g1^sr
        Mcl.mul(t_dash, g1, sr)
        Mcl.mul(temp_g, sigma_roof, sm)
        Mcl.add(t_dash, t_dash, temp_g)
        Mcl.neg(tmp, e)
        Mcl.mul(temp_g, sigma_plane, tmp)
        Mcl.add(t_dash, t_dash, temp_g)
        val first = GT()
        val second = GT()
        Mcl.pairing(first, sigma_plane, g2)
        Mcl.pairing(second, sigma_roof, gx)

        //binding.textviewFirst.setText("t verified:" + t.equals(t_dash));
        //binding.textView2.setText("Pairing verified:" + first.equals(second));
        return if (t.equals(t_dash) && first.equals(second)) {
            Toast.makeText(context, "Working", Toast.LENGTH_SHORT).show()
            return "accp/SUCCESS/$hash"
        } else {
            Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show()
            return ""
        }
    }



    private fun mclCurvePointToDatabase(curvePoint: G1): String {
        val curvePointMcl = curvePoint.toString()
        var x_decimal_string = ""
        var y_decimal_string = ""
        var indexHolder = 2
        for (i in 2 until curvePointMcl.length) {
            if (curvePointMcl.substring(i, i + 1) != " ") {
                x_decimal_string += curvePointMcl.substring(i, i + 1)
                indexHolder++
            } else {
                indexHolder++
                break
            }
        }
        for (i in indexHolder until curvePointMcl.length) {
            y_decimal_string += curvePointMcl.substring(i, i + 1)
        }
        val x_decimal = BigInteger(x_decimal_string)
        val y_decimal = BigInteger(y_decimal_string)
        var x_hex = x_decimal.toString(16)
        var y_hex = y_decimal.toString(16)
        if (x_hex.length < 64) {
            var prefix_x = ""
            val number_of_zeros = 64 - x_hex.length
            for (i in 0 until number_of_zeros) {
                prefix_x += "0"
            }
            x_hex = prefix_x + x_hex
        }
        if (y_hex.length < 64) {
            var prefix_y = ""
            val number_of_zeros = 64 - y_hex.length
            for (i in 0 until number_of_zeros) {
                prefix_y += "0"
            }
            y_hex = prefix_y + y_hex
        }
        return "04" + x_hex.uppercase() + y_hex.uppercase()
    }

    fun toHex(arg: String): String {
        return String.format("%x", BigInteger(1, arg.toByteArray(StandardCharsets.UTF_8)))
    }

    companion object {
        internal const val BN256_g1_hex =
            "1 2523648240000001BA344D80000000086121000000000013A700000000000012 0000000000000000000000000000000000000000000000000000000000000001"

        internal fun SHA256(input: String): String {
            return MessageDigest
                .getInstance("SHA-256")
                .digest(input.toByteArray())
                .fold("", { str, it -> str + "%02X".format(it) })
        }

        private fun SHA1(input: String): String {
            var md: MessageDigest? = null
            try {
                md = MessageDigest.getInstance("SHA-1")
            } catch (e: NoSuchAlgorithmException) {
                e.printStackTrace()
            }
            md!!.update(hexStringToByteArray(input))
            val digest = md.digest()
            return byteArrayToHexString(digest).uppercase()
        }

        //ByteArray -> Hexadecimal String
        fun byteArrayToHexString(byteArray: ByteArray): String {
            val hexStringBuffer = StringBuffer()
            hexStringBuffer.append(byteArray.toHex())
            return hexStringBuffer.toString()
        }

        fun hexStringToByteArray(hexString: String): ByteArray {
            require(hexString.length % 2 != 1) { "Invalid hexadecimal String supplied." }
            val bytes = ByteArray(hexString.length / 2)
            var i = 0
            while (i < hexString.length) {
                bytes[i / 2] = hexToByte(hexString.substring(i, i + 2))
                i += 2
            }
            return bytes
        }

        fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }

        fun String.decodeHex(): ByteArray {
            check(length % 2 == 0) { "Must have an even length" }

            return chunked(2)
                .map { it.toInt(16).toByte() }
                .toByteArray()
        }

        fun hexToByte(hexString: String): Byte {
            val firstDigit = toDigit(hexString[0])
            val secondDigit = toDigit(hexString[1])
            return ((firstDigit shl 4) + secondDigit).toByte()
        }

        private fun toDigit(hexChar: Char): Int {
            val digit = Character.digit(hexChar, 16)
            require(digit != -1) { "Invalid Hexadecimal Character: $hexChar" }
            return digit
        }

    }

    init {
        this.context = context!!.applicationContext
    }

    init {
        System.loadLibrary("mcljava")
    }
}