/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */
package com.herumi.mcl

import kotlin.jvm.JvmOverloads
import com.herumi.mcl.MclJNI
import kotlin.jvm.Synchronized
import com.herumi.mcl.Fp
import com.herumi.mcl.Fr
import com.herumi.mcl.G1
import com.herumi.mcl.G2
import com.herumi.mcl.GT
import com.herumi.mcl.MclConstants
import com.herumi.mcl.SWIGTYPE_p_bool
import com.herumi.mcl.ElgamalJNI
import com.herumi.mcl.CipherText

class PrivateKey protected constructor(
    @field:Transient private var swigCPtr: Long,
    @field:Transient protected var swigCMemOwn: Boolean
) {
    protected fun finalize() {
        delete()
    }

    @Synchronized
    fun delete() {
        if (swigCPtr != 0L) {
            if (swigCMemOwn) {
                swigCMemOwn = false
                ElgamalJNI.delete_PrivateKey(swigCPtr)
            }
            swigCPtr = 0
        }
    }

    fun toStr(): String? {
        return ElgamalJNI.PrivateKey_toStr(swigCPtr, this)
    }

    override fun toString(): String {
        return ElgamalJNI.PrivateKey_toString(swigCPtr, this)!!
    }

    fun fromStr(str: String?) {
        ElgamalJNI.PrivateKey_fromStr(swigCPtr, this, str)
    }

    fun save(fileName: String?) {
        ElgamalJNI.PrivateKey_save(swigCPtr, this, fileName)
    }

    fun load(fileName: String?) {
        ElgamalJNI.PrivateKey_load(swigCPtr, this, fileName)
    }

    fun init() {
        ElgamalJNI.PrivateKey_init(swigCPtr, this)
    }

    val publicKey: PublicKey
        get() = PublicKey(ElgamalJNI.PrivateKey_getPublicKey(swigCPtr, this), true)

    fun dec(c: CipherText?, b: SWIGTYPE_p_bool?): Int {
        return ElgamalJNI.PrivateKey_dec__SWIG_0(
            swigCPtr,
            this,
            CipherText.getCPtr(c),
            c,
            SWIGTYPE_p_bool.getCPtr(b)
        )
    }

    fun dec(c: CipherText?): Int {
        return ElgamalJNI.PrivateKey_dec__SWIG_1(swigCPtr, this, CipherText.getCPtr(c), c)
    }

    fun setCache(rangeMin: Int, rangeMax: Int) {
        ElgamalJNI.PrivateKey_setCache(swigCPtr, this, rangeMin, rangeMax)
    }

    fun clearCache() {
        ElgamalJNI.PrivateKey_clearCache(swigCPtr, this)
    }

    constructor() : this(ElgamalJNI.new_PrivateKey(), true) {}

    companion object {
        protected fun getCPtr(obj: PrivateKey?): Long {
            return obj?.swigCPtr ?: 0
        }
    }
}