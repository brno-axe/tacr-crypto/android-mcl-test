/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.2
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */
package com.herumi.mcl

import kotlin.jvm.JvmOverloads
import com.herumi.mcl.MclJNI
import kotlin.jvm.Synchronized
import com.herumi.mcl.Fp
import com.herumi.mcl.Fr
import com.herumi.mcl.G1
import com.herumi.mcl.G2
import com.herumi.mcl.GT
import com.herumi.mcl.MclConstants
import com.herumi.mcl.SWIGTYPE_p_bool
import com.herumi.mcl.ElgamalJNI
import com.herumi.mcl.CipherText

class G2 protected constructor(
    @field:Transient private var swigCPtr: Long,
    @field:Transient protected var swigCMemOwn: Boolean
) {
    protected fun finalize() {
        delete()
    }

    @Synchronized
    fun delete() {
        if (swigCPtr != 0L) {
            if (swigCMemOwn) {
                swigCMemOwn = false
                MclJNI.delete_G2(swigCPtr)
            }
            swigCPtr = 0
        }
    }

    constructor() : this(MclJNI.new_G2__SWIG_0(), true) {}
    constructor(rhs: G2?) : this(MclJNI.new_G2__SWIG_1(getCPtr(rhs), rhs), true) {}
    constructor(
        ax: Fp?,
        ay: Fp?,
        bx: Fp?,
        by: Fp?
    ) : this(
        MclJNI.new_G2__SWIG_2(
            Fp.getCPtr(ax),
            ax,
            Fp.getCPtr(ay),
            ay,
            Fp.getCPtr(bx),
            bx,
            Fp.getCPtr(by),
            by
        ), true
    ) {
    }

    fun equals(rhs: G2?): Boolean {
        return MclJNI.G2_equals(swigCPtr, this, getCPtr(rhs), rhs)
    }

    val isZero: Boolean
        get() = MclJNI.G2_isZero(swigCPtr, this)

    operator fun set(ax: Fp?, ay: Fp?, bx: Fp?, by: Fp?) {
        MclJNI.G2_set(
            swigCPtr,
            this,
            Fp.getCPtr(ax),
            ax,
            Fp.getCPtr(ay),
            ay,
            Fp.getCPtr(bx),
            bx,
            Fp.getCPtr(by),
            by
        )
    }

    fun clear() {
        MclJNI.G2_clear(swigCPtr, this)
    }

    fun setStr(str: String?, base: Int) {
        MclJNI.G2_setStr__SWIG_0(swigCPtr, this, str, base)
    }

    fun setStr(str: String?) {
        MclJNI.G2_setStr__SWIG_1(swigCPtr, this, str)
    }

    fun toString(base: Int): String? {
        return MclJNI.G2_toString__SWIG_0(swigCPtr, this, base)
    }

    override fun toString(): String {
        return MclJNI.G2_toString__SWIG_1(swigCPtr, this)!!
    }

    fun deserialize(cbuf: ByteArray?) {
        MclJNI.G2_deserialize(swigCPtr, this, cbuf)
    }

    fun serialize(): ByteArray? {
        return MclJNI.G2_serialize(swigCPtr, this)
    }

    fun normalize() {
        MclJNI.G2_normalize(swigCPtr, this)
    }

    companion object {
        fun getCPtr(obj: G2?): Long {
            return obj?.swigCPtr ?: 0
        }
    }
}