package com.example.wbb

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

val Context.settingsDataStore: DataStore<Preferences> by preferencesDataStore(name = "crypto")

object DataStoreManager {

    suspend fun saveValue(context: Context, key: Preferences.Key<String>, value: String) {
        context.settingsDataStore.edit {
            it[key] = value
        }
    }

    suspend fun getValue(context: Context, key: Preferences.Key<String>, default: String = ""): String {
        val valueFlow: Flow<String> = context.settingsDataStore.data.map {
            it[key] ?: default
        }
        return valueFlow.first()
    }
    val G1 = stringPreferencesKey("g1")
    val G2 = stringPreferencesKey("g2")
    val M1 = stringPreferencesKey("m1")
    val M2 = stringPreferencesKey("m2")
    val X0 = stringPreferencesKey("x0")
    val X1 = stringPreferencesKey("x1")
    val X2 = stringPreferencesKey("x2")
    val H0 = stringPreferencesKey("h0")
    val H1 = stringPreferencesKey("h1")
    val H2 = stringPreferencesKey("h2")
    val SIGMA = stringPreferencesKey("sigma")
    val SIGMA_X0 = stringPreferencesKey("sigma_x0")
    val SIGMA_X1 = stringPreferencesKey("sigma_x1")
    val SIGMA_X2 = stringPreferencesKey("sigma_x2")

    //temp
    val T = stringPreferencesKey("t")
    val SM = stringPreferencesKey("sm")
    val SV = stringPreferencesKey("sv")
    val SIGMA_ROOF = stringPreferencesKey("sigma_roof")
    val NONCE = stringPreferencesKey("nonce")

}