package com.example.wbb.ui.benchmark

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.example.wbb.CryptoCore
import com.example.wbb.databinding.FragmentBenchmarkBinding
import com.herumi.mcl.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class BenchmarkFragment : Fragment() {
    private var binding: FragmentBenchmarkBinding? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBenchmarkBinding.inflate(inflater, container, false)

        binding!!.buttonBLS381.setOnClickListener{
            val results = mutableListOf<String>()

            results.add(frBench(MclConstants.BLS12_381))
            results.add(g1Bench(MclConstants.BLS12_381))
            results.add(g2Bench(MclConstants.BLS12_381))
            results.add(gtBench(MclConstants.BLS12_381))
            results.add(pairBench(MclConstants.BLS12_381))

            val adapter = context?.let { it1 -> ArrayAdapter<String>(it1, android.R.layout.simple_list_item_1, results) }
            binding!!.listView.adapter = adapter

        }

        binding!!.buttonBN254.setOnClickListener{
            val results = mutableListOf<String>()

            results.add(frBench(MclConstants.BN254))
            results.add(g1Bench(MclConstants.BN254))
            results.add(g2Bench(MclConstants.BN254))
            results.add(gtBench(MclConstants.BN254))
            results.add(pairBench(MclConstants.BN254))

            val adapter = context?.let { it1 -> ArrayAdapter<String>(it1, android.R.layout.simple_list_item_1, results) }
            binding!!.listView.adapter = adapter
        }



        return binding!!.root
    }

    fun frBench(curveType: Int): String {
        Mcl.SystemInit(curveType)
        val startTime = System.nanoTime()

        val fr1 = Fr()
        val fr2 = Fr()
        var secTime: Long

        secTime = System.nanoTime()
        Mcl.add(fr1, fr1, fr2)
        logTime("Fr/add", secTime)

        secTime = System.nanoTime()
        Mcl.sub(fr1,fr1,fr2)
        logTime("Fr/sub", secTime)

        secTime = System.nanoTime()
        Mcl.mul(fr1,fr1,fr2)
        logTime("Fr/mul", secTime)

        secTime = System.nanoTime()
        Mcl.div(fr1,Fr(1),  fr1)
        logTime("Fr/div", secTime)

        secTime = System.nanoTime()
        Mcl.neg(fr1,fr1)
        logTime("Fr/neg", secTime)

        val endTime = System.nanoTime()

        return "Fr benchmark done in: " +  TimeUnit.MICROSECONDS.convert(endTime -
                startTime, TimeUnit.NANOSECONDS).toString() + " \tµs"
    }

    fun g1Bench(curveType: Int): String {
        Mcl.SystemInit(curveType)
        val startTime = System.nanoTime()

        val g1 = G1()
        val g2 = G1()
        val fr = Fr()

        var secTime = System.nanoTime()
        Mcl.add(g1, g1, g2)
        logTime("G1/add", secTime)

        secTime = System.nanoTime()
        Mcl.sub(g1,g1,g2)
        logTime("G1/sub", secTime)

        secTime = System.nanoTime()
        Mcl.mul(g1,g1,fr)
        logTime("G1/mul", secTime)

        secTime = System.nanoTime()
        Mcl.dbl(g1, g1)
        logTime("G1/dbl", secTime)

        secTime = System.nanoTime()
        Mcl.neg(g1,g1)
        logTime("G1/neg", secTime)

        val endTime = System.nanoTime()

        return "G1 benchmark done in: " +  TimeUnit.MICROSECONDS.convert(endTime -
                startTime, TimeUnit.NANOSECONDS).toString() + " \tµs"
    }

    fun g2Bench(curveType: Int): String {
        Mcl.SystemInit(curveType)
        val startTime = System.nanoTime()

        val g1 = G2()
        val g2 = G2()
        val fr = Fr()

        var secTime = System.nanoTime()
        Mcl.add(g1, g1, g2)
        logTime("G2/add", secTime)

        secTime = System.nanoTime()
        Mcl.sub(g1,g1,g2)
        logTime("G2/sub", secTime)

        secTime = System.nanoTime()
        Mcl.mul(g1,g1,fr)
        logTime("G2/mul", secTime)

        secTime = System.nanoTime()
        Mcl.dbl(g1, g1)
        logTime("G2/dbl", secTime)

        secTime = System.nanoTime()
        Mcl.neg(g1,g1)
        logTime("G2/neg", secTime)

        val endTime = System.nanoTime()

        return "G2 benchmark done in: " +  TimeUnit.MICROSECONDS.convert(endTime -
                startTime, TimeUnit.NANOSECONDS).toString() + " \tµs"
    }

    fun gtBench(curveType: Int): String {
        Mcl.SystemInit(curveType)
        val startTime = System.nanoTime()

        val g1 = GT()
        val g2 = GT()
        val fr = Fr()

        var secTime = System.nanoTime()
        Mcl.pow(g1, g1, fr)
        logTime("GT/pow", secTime)

        secTime = System.nanoTime()
        Mcl.mul(g1,g1,g2)
        logTime("GT/mul", secTime)

        /*
        val g3 = GT()
        secTime = System.nanoTime()
        Mcl.inv(g3,g3)
        logTime("GT/inv", secTime)
        */
        val endTime = System.nanoTime()

        return "GT benchmark done in: " +  TimeUnit.MICROSECONDS.convert(endTime -
                startTime, TimeUnit.NANOSECONDS).toString() + " \tµs"
    }

    fun pairBench(curveType: Int): String {
        Mcl.SystemInit(curveType)
        val startTime = System.nanoTime()

        val g1 = G1()
        val g2 = G2()
        val gt = GT()

        var secTime = System.nanoTime()
        Mcl.pairing(gt, g1, g2)
        logTime("Pairing", secTime)

        val endTime = System.nanoTime()
        return "GT benchmark done in: " +  TimeUnit.MICROSECONDS.convert(endTime -
                startTime, TimeUnit.NANOSECONDS).toString() + " \tµs"
    }

    fun logTime(tag: String, secTime: Long) {
        Log.d(tag + "", (System.nanoTime() - secTime).toString() + " ns")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    init {
        System.loadLibrary("mcljava")
    }


}