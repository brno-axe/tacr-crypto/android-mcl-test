## Name
Android MCL Test

## Description
The application was created by Jan Hlinka as a part of his semestral thesis which was supervised by Petr Dzurenda. The main purpose of this application is to benchmark the MCL cryptographic library (https://github.com/herumi/mcl) on the Android platform, and therefore, to show the speed and efficiency of crucial arithmetic operations needed for cryptographic protocol design. Brno University of Technology, Czech Republic, Brno AXE group, https://axe.vut.cz/.

## SW Requirements
- [ ] [minSdk 21]
- [ ] [targetSdk 31]

## Installation
The application has to be built in order to get the installation APK file, for example by using the Android Studio development tool. Android devices have to have allowed the installation of apps from unknown sources.

## Usage
The benchmark results are shown in form of logs in the terminal, therefore, the Android device has to by connected to the debugger, for example, Android Studio development tool.

## Acknowledgment
This application is partly supported in part by European Union\'s Horizon 2020 research and innovation program under grant agreement No 830892, project SPARTA, and in part by the Ministry of the Interior of the Czech Republic under grant VJ01030002.

## License
Modified new BSD License http://opensource.org/licenses/BSD-3-Clause

The MCL library contains some part of the followings software licensed by BSD-3-Clause.

- [ ] [xbyak]
- [ ] [cybozulib]
- [ ] [Lifted-ElGamal]
